package by.home.servlet;

import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.comment.Comment;
import com.atlassian.bitbucket.comment.CommentChain;
import com.atlassian.bitbucket.pull.PullRequestAction;
import com.atlassian.bitbucket.pull.PullRequestActivity;
import com.atlassian.bitbucket.pull.PullRequestCommentActivity;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Named("ReportsServlet")
public class ReportsServlet extends HttpServlet {
    private static final String URL_DELIMITER = "reports/";
    private static final int AVATAR_SIZE = 48;
    private static final int PROJECT_INDEX_IN_URL = 0;
    private static final int REPOSITORY_INDEX_IN_URL = 1;
    private static final int PULL_REQUEST_ID_INDEX_IN_URL = 2;
    private static final Logger log = LoggerFactory.getLogger(ReportsServlet.class);

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final RepositoryService repositoryService;
    private final PullRequestService pullRequestService;
    private final AvatarService avatarService;

    @Inject
    public ReportsServlet(@ComponentImport SoyTemplateRenderer soyTemplateRenderer,
                          @ComponentImport RepositoryService repositoryService,
                          @ComponentImport PullRequestService pullRequestService,
                          @ComponentImport AvatarService avatarService) {
        this.repositoryService = repositoryService;
        this.pullRequestService = pullRequestService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.avatarService = avatarService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] splittedUrl = req.getRequestURL().toString().split(URL_DELIMITER)[1].split("/");
        String projectId = splittedUrl[PROJECT_INDEX_IN_URL];
        String repositoryId = splittedUrl[REPOSITORY_INDEX_IN_URL];
        int pullRequestId = Integer.parseInt(splittedUrl[PULL_REQUEST_ID_INDEX_IN_URL]);
        Repository repository = repositoryService.getBySlug(projectId, repositoryId);
        Map<String, Object> map = new HashMap<>();
        map.put("pullRequest", pullRequestService.getById(repository.getId(), pullRequestId));
        map.put("repository", repository);
        map.put("data", returnCommentators(repository, pullRequestId));
        render(resp, map);
    }

    /**
     * Aggregates all corresponding commentaries for current pull request, extracts the list of commentators
     * with their comments count and parses it into serializable list.
     *
     * @return
     */
    private List<Map<String, Object>> returnCommentators(Repository repository, int pullRequestId) {

        Page<PullRequestActivity> page = pullRequestService.getActivities(
                repository.getId(),
                pullRequestId,
                new PageRequestImpl(0, PageRequestImpl.MAX_PAGE_LIMIT));

        List<Comment> rootComments = new ArrayList<>();
        List<Map<String, Object>> result = new ArrayList<>();
        AvatarRequest avatarRequest = new AvatarRequest(false, AVATAR_SIZE);

        page.getValues().forEach(activity -> {
            if (activity.getAction() == PullRequestAction.COMMENTED) {
                rootComments.add(((PullRequestCommentActivity) activity).getComment());
            }
        });

        CommentChain.of(rootComments).stream()
                .collect(Collectors.groupingBy(Comment::getAuthor))
                .forEach((user, comments) -> {
                            Map<String, Object> booferMap = new HashMap<>();
                            booferMap.put("displayName", user.getDisplayName());
                            booferMap.put("comments", comments.size());
                            booferMap.put("avatarUrl", avatarService.getUrlForPerson(user, avatarRequest));
                            result.add(booferMap);
                        }
                );

        return result;
    }

    private void render(HttpServletResponse resp, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(),
                    "by.home.Task:reports-soy",
                    "plugin.reports.reportsTab",
                    data);
        } catch (SoyException e) {
            log.warn("Error during rendering the soy template.", e);
            throw new ServletException(e);
        }
    }
}