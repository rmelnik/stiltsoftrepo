define('stiltsoft/test-task/widgets-renderer', function () {

  var onReady = function (data) {

    var PIXELS_PER_PERCENT_WIDTH = screen.width / 100;
    var PIXELS_PER_PERCENT_HEIGHT = screen.height / 100;
    var WIDGET_WIDTH = PIXELS_PER_PERCENT_WIDTH * 14.0625;
    var MARGIN_OFFSETS = PIXELS_PER_PERCENT_WIDTH * 0.2604;

    var ROW_HEIGHT = 24;
    var ROW_OFFSET = 27;

    var AVATAR_OFFSET = -25;
    var AVATAR_SIZE = 16;
    var AVATAR_BAR_OFFSET_X = 27;
    var AVATAR_BAR_OFFSET_Y = 28;
    var AVATAR_BAR_WIDTH = 20;

    var TOOLTIP_OFFSET_Y = 56;

    var NAME_BAR_OFFSET_Y = 17;
    var NAME_BAR_OFFSET_X = 20;

    var CONTENT_HEIGHT = ROW_HEIGHT * data.length;

    var MASK_DIAMETER = 8;

    var CHART_BAR_OFFSET_X = 0;

    var NAME_BAR_WIDTH = PIXELS_PER_PERCENT_WIDTH * 6.7708;

    d3.select('.comments .content')
      .text(d3.sum(data, function (d) {
        return (d.comments)
      }));

    var margin = {top: MARGIN_OFFSETS, right: MARGIN_OFFSETS, bottom: MARGIN_OFFSETS, left: (WIDGET_WIDTH / 2)},
      width = WIDGET_WIDTH - margin.left - margin.right,
      height = PIXELS_PER_PERCENT_HEIGHT * 5.0925 - margin.top - margin.bottom;

    var y = d3.scaleBand()
      .range([height, 0])
      .padding(0.1);

    var x = d3.scaleLinear()
      .range([0, width]);

    var svg = d3.select(".comments-per-user .content")
      .data(data)
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", CONTENT_HEIGHT)
      .append("g")
      .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

    data.forEach(function (d) {
      d.comments = +d.comments;
    });

    x.domain([0, d3.max(data, function (d) {
      return d.comments;
    })]);
    y.domain(data.map(function (d) {
      return d.displayName;
    }));

    var tooltip = d3.select(".comments-per-user").append("div")
      .attr("class", "chart-tooltip");

    svg.selectAll(".bar")
      .data(data)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("width", function (d) {
        return x(d.comments);
      })
      .attr('transform', function (d, i) {
        return 'translate(' + CHART_BAR_OFFSET_X + ',' + ROW_OFFSET * i + ')';
      })
      .attr("height", y.bandwidth())
      .on("mouseover", function (d) {
        tooltip.transition()
          .duration(200)
          .style("opacity", .9);
        tooltip.html(d.comments + ' comments')
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - TOOLTIP_OFFSET_Y) + "px");
      })
      .on("mouseout", function (d) {
        tooltip.transition()
          .duration(500)
          .style("opacity", 0);
      });

    d3.selectAll('.tick').remove();
    d3.selectAll('path.domain').remove();

    d3.select('svg')
      .append('svg')
      .classed('name-bar', true)
      .attr('width', NAME_BAR_WIDTH);

    d3.select(".name-bar")
      .selectAll("text")
      .data(data)
      .enter()
      .append("text")
      .text(function (d) {
        return d.displayName;
      })
      .attr('transform', function (d, i) {
        return 'translate(' + NAME_BAR_OFFSET_X + ',' + (NAME_BAR_OFFSET_Y + (ROW_OFFSET * i)) + ')';
      });

    d3.select('svg')
      .append('svg')
      .classed('avatar-bar', true)
      .attr('width', AVATAR_BAR_WIDTH);

    d3.select('.avatar-bar').selectAll('image')
      .data(data).enter()
      .append('svg:image')
      .classed('avatar-image', true)
      .attr("xlink:href", function (d) {
        return d.avatarUrl;
      })
      .attr("x", AVATAR_OFFSET)
      .attr("y", AVATAR_OFFSET)
      .attr('clip-path', 'url(#cut-off-avatar)')
      .attr("height", AVATAR_SIZE)
      .attr("width", AVATAR_SIZE)
      .attr('transform', function (d, i) {
        return 'translate(' + AVATAR_BAR_OFFSET_X + ',' + (AVATAR_BAR_OFFSET_Y + (ROW_OFFSET * i)) + ')';
      });

    d3.select('svg')
      .append("defs")
      .append("clipPath")
      .attr("id", "cut-off-avatar")
      .append('circle')
      .attr("cx", MASK_DIAMETER)
      .attr("cy", MASK_DIAMETER)
      .attr("r", MASK_DIAMETER - 1)
      .attr('transform', 'translate(' + AVATAR_OFFSET + ',' + AVATAR_OFFSET + ')');

  };

  return {
    onReady: onReady
  }
});